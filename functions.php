<?php

/**
 * Les add_action permettent de faire tout et n'importe quoi via
 * wordpress selon le hook (premier argument) qu'on utilise.
 * En gros c'est une manière de dire à wordpress "exécute cette fonction à tel moment",
 * la fonction à exécuter est le deuxième argument, le "quand l'exécuter" est le hook
 */
add_action('wp_enqueue_scripts', 'parent_style');

//ici on fait une fonction qui va charger le fichier style.css du theme parent
function parent_style() {
    wp_enqueue_style('parent-style', 
                    get_template_directory_uri() . '/style.css');
}

add_action('customize_register', 'customizer_enhanced');
/**
 * Ici, on fait une fonction qui va rajouter de nouveaux éléments au
 * customizer de notre thème
 */
function customizer_enhanced( WP_Customize_Manager $wp_customize ) {
    //On rajoute une nouvelle section au customizer
    $wp_customize->add_section('simplon_section', [
        'title' => 'Simplon Option',
        'priority' => 30
    ]);
    //On crée un nouveau "setting", c'est un peu comme une variable utilisable par wordpress
    $wp_customize->add_setting('site_navigation_bg', [
        'default' => '#FFF',
        'transport' => 'refresh'
    ]);
    /*
    On crée un nouveau "control", un input en quelque sorte, et on lui indique quel
    type de control ça doit être (ici un color picker), à quel setting on veut le lié,
    dans quel section on veut le mettre et le label qu'il aura
    */
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'nav_bg', [
        'section' => 'simplon_section',
        'label' => 'Primary Menu Background',
        'settings' => 'site_navigation_bg'
    ]));


    $wp_customize->add_setting('header_text', [
        'default' => 'Simplon',
        'transport' => 'refresh'
    ]);

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'header_text_control', [
        'section' => 'simplon_section',
        'label' => 'Header Text',
        'settings' => 'header_text'
    ]));
}

add_action('wp_head', 'customizer_style');
/**
 * On fait ici une fonction qui va afficher une balise style, on va
 * s'en servir pour mettre les styles éditables via le customizer afin
 * de pouvoir y injecter les settings de celui ci.
 */
function customizer_style() {
    ?>
<style>
    #site-navigation {
        /* On utilise ici notre setting site_navigation_bg qu'on assigne
        en bgColor d'un sélecteur */
        background-color: <?php echo get_theme_mod('site_navigation_bg') ?>;
    }

</style>

    <?php
}
